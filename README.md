# Invio ma non consumo  
Un tastierino di controllo a bassissimo consumo  
<img src="https://gitlab.com/poggiolevante/invio-ma-non-consumo/-/wikis/uploads/e573f2ae962783ae2b1508b0b263ca4e/Circuito.jpg" width="50%" height="50%">

## Specifiche del progetto
Il progetto ci è stato commissionato da una azienda alla ricerca di una soluzione non intrusiva, ovvero senza collegamenti, per il controllo e il monitoraggio di basi di trasmissioni radiofoniche e televisive.

## Cosa abbiamo costruito
Abbiamo proposto un **tastierino** dotato di **sensori magnetici** per il rilevamento della apertura, previa immissione di codice numerico di autenticazione, di **porte e cancelli** e dotato di connessione alla **rete 0G SigFox** per l'invio degli eventi a un server remoto.  
La rete SigFox consente l'invio di messaggi di pochi byte, con un impatto energetico molto più basso rispetto alla rete cellulare tradizionale. Il progetto è stato designato per l'uso occasionale in località (remote) **senza connessione** Internet, **senza copertura** della rete cellulare e **senza corrente**. È stato quindi prioritario il design rivolto al risparmio energetico.

## Risultati e sviluppi futuri

La prima versione su breadboard ha avuto dei problemi di contatti che impedivano lo spegnimento automatico del dispositivo, problema risolto nella versione preliminare del PCB.

Allo stato attuale, il dispositivo funziona, ma deve essere ulteriormente rifinito e ottimizzato: E’ necessario creare una seconda versione del circuito stampato, per risolvere gli errori commessi nel primo; inoltre, il software può essere ottimizzato per maggiore stabilità e velocità e un minore impatto energetico.

## Open source e documentazione
Questo progetto è open source: Chiunque può scaricare i file necessari, ricreare il progetto e contribuire al suo miglioramento.  
I file necessari per ricreare il progetto si trovano [su GitLab.](https://gitlab.com/poggiolevante/invio-ma-non-consumo)
, mentre la documentazione si trova [nella wiki di GitLab.](https://gitlab.com/poggiolevante/invio-ma-non-consumo/-/wikis/home)
Viene utilizzata la [licenza MIT.](https://gitlab.com/poggiolevante/invio-ma-non-consumo/-/blob/main/LICENSE)
