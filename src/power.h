#include <Adafruit_INA219.h>

class Power {
  
private:

  void readIna();
  float battery_voltage = 0;
  float battery_amps = 0;
  Adafruit_INA219 ina219;

  const float min_volt=3.1;
  const float max_volt=4.2;
  const float low_volt=3.4;
public:

  Power(){}
  void begin();

  float getVoltage();
  float getAmps();
  bool onBattery();
  bool isLowBattery();
 
};
