typedef struct __attribute__ ((packed)) fullpacket {
  uint8_t status;
  uint32_t timeStamp;
} fullpacket;

fullpacket packet;

void preparePacket(){
   packet.status = sys_status;
   // Lettura del timestamp dell'accensione
   DS3231_get(&t);
   //Serial.println(t.unixtime);
   packet.timeStamp = Swap4Bytes(t.unixtime);
   if (isDoorOpen()) packet.status = packet.status + 16;
   // Start the module
}

bool inviodati(bool full){
  //Prepariamo il messaggio

  if (DEBUG) {
    if (full) {Serial.println("Invio pacchetto"); } 
    else { Serial.println("Invio pacchetto breve"); }
  }
  preparePacket();
  
  // Start the module
  SigFox.begin();
  // Wait at least 30mS after first configuration (100mS before)
  delay(100);
  // Clears all pending interrupts
  SigFox.status();
  delay(1);

  //Apriamo la trasmissione
  SigFox.beginPacket();
 
  SigFox.write((uint8_t*)&packet, full ? 5 : 1);
 
  int ret = SigFox.endPacket();  // send buffer to SIGFOX network

  
  if (DEBUG){
      if (ret > 0) {
    Serial.print ("Errore (?): ");
    Serial.println(ret);
  } else {
    Serial.println("Dati trasmessi correttamente");
  }
   Serial.print("Stato SigFox: ");
   Serial.println(SigFox.status(SIGFOX));
   Serial.print("Stato Atmel: ");
   Serial.println(SigFox.status(ATMEL));
  }


  SigFox.end();
  return ret;
}

bool sigfoxinit() {
  if (!SigFox.begin()) {
    Serial.println("Shield error or not present!");
    return false;
  }
  
  // Enable debug led and disable automatic deep sleep
  // Comment this line when shipping your project :)
  SigFox.debug();

  SigFox_version = SigFox.SigVersion();
  SigFox_ID = SigFox.ID();
  SigFox_PAC = SigFox.PAC();

  // Display module informations
  Serial.println("SigFox FW version " + SigFox_version);
  Serial.println("ID  = " + SigFox_ID);
  Serial.println("PAC = " + SigFox_PAC);
  SigFox.end();
  return true;
}
