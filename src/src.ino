

//Inclusioni di librerie

#include <ArduinoLowPower.h>
#include <IoAbstraction.h>
#include <IoAbstractionWire.h>
#include <EepromAbstractionWire.h>
#include <SigFox.h>
#include <Wire.h>
#include "config.h"
#include <ds3231.h>
#include <Adafruit_INA219.h>

#include "power.h"


//Definizioni globali
// DEBUG a 1: Messaggi informativi
// DEBUG a 2: Messaggi di trace di tutte le funzioni
#define DEBUG 2
// Porta su cui stampare i messaggi
#define Serial Serial1
#define IOExp_address 0x20
#define EEPROM_address 0x57
#define pinbuzzer 0
#define pindoor 1
#define pinready 2
//Pinled a 1: Led verde
//Pinled a 0: Led rosso
#define pinled 3
#define pinshutdown 5
uint16_t TIMEOUT = 15000;
#define KEEPALIVE_INTERVAL 60
#define KEEPALIVE_ITERATIONS 60
//Indirizzi di memoria
#define mem_time 0
#define mem_status 4
#define mem_keepalive 5




//Dichiarazione della tabella degli stati
enum status : byte { NOTDEFINED, NORMAL, KEEPALIVE, NOCODE, PARTCODE, CORRCODE,  ALARM, MANINSIDE, MANOUTSIDE };


// Variabili globali
bool found = false;
bool print = true;
uint32_t boot_time;
uint32_t last_boot_time;
int i = 0;
int c = 0;
int lastc, lastr;
String SigFox_version;
float battery_voltage;
String SigFox_ID;
String SigFox_PAC;
String codice = "";
char key;
byte lastkey = 0;
status sys_status = NOTDEFINED;
ts t;
unsigned long millid = 0;
bool enable_timeout = false;
bool valid_code = true;
int numbers = 0;
bool is_keepalive_boot = false;
uint32_t eeprom_time = 0;
uint32_t deltat = 0;
String payload = "";
//Adafruit_INA219 ina219;
Power power;

// Istanze
// Create both an Arduino and an IO expander based IO abstraction
IoAbstractionRef ioExpander = ioFrom8574(IOExp_address);
I2cAt24Eeprom eeprom(EEPROM_address, PAGESIZE_AT24C32);


void setup() {

  if (DEBUG) {
    Serial.begin(115200);
    Serial.println();
    Serial.println("Seriale inizializzata");
  }

  // Inizializzazione primi sottosistemi
  Wire.begin();
  DS3231_init(DS3231_INTCN);

  // Lettura del timestamp dell'accensione
  DS3231_get(&t);
  #ifdef INFO
    echo_now();
  #endif
  boot_time = t.unixtime;


  //Impostazione della modalità dei pin
  pinMode(pinready, OUTPUT);
  pinMode(pindoor, INPUT_PULLUP);
  pinMode(pinshutdown, OUTPUT);
  pinMode(pinbuzzer, OUTPUT);
  pinMode(pinled, OUTPUT);;
  digitalWrite (pinled, 0);
  digitalWrite(pinshutdown, LOW);

  if (DEBUG > 1) Serial.println("PIN impostati");

  // Inizializzazione altri sottosistemi
  power.begin();
  Serial.print("Tensione batteria: ");
  Serial.println(power.getVoltage());
  //Serial.println(power.onBattery() ? "Battery" : "AC");
  expanderinit();
  sigfoxinit();
  eeprom_time = eeprom.read32(0);
  sys_status = loadStatus();

  is_keepalive_boot = is_keepalive();
  deltat = boot_time - eeprom_time;

  // Notifichiamo l'utente dell'avvenuta accensione
  if (!is_keepalive_boot) {
    tone_startup();
    digitalWrite (pinready, 1);
    //Attiviamo il meccanismo di timeout dell'input
    enable_timeout = true;
    if (sys_status == KEEPALIVE) sys_status = NORMAL;
    eeprom.write8(mem_keepalive, 0);
  } else {
    sys_status = KEEPALIVE;
  }

  if ( DEBUG ) {
    Serial.print("Porta aperta: ");
    Serial.println(isDoorOpen() ? "si" : "no");
    reset_timeout();
    Serial.print("Dispositivo avviato in ");
    Serial.print(millis());
    Serial.println(" millisecondi.");
    Serial.print("Stato: ");
    Serial.println(sys_status, HEX);
  }

}


void loop() {

  switch (sys_status) {
    case NOTDEFINED:
    case NORMAL:
    case PARTCODE:
    case NOCODE:
    case MANOUTSIDE:
      if (!is_keepalive_boot) {
        input_handler();
        if (timeout() && enable_timeout ) {
          if (DEBUG) {
            Serial.print("\nTimeout raggiunto, ");
            if (sys_status == PARTCODE) {
              Serial.println("codice parzialmente inserito");
            } else if (sys_status == NOCODE) {
              Serial.println("nessun codice inserito");
            }
          }
          if (sys_status != PARTCODE) sys_status = NOCODE;
          system_shutdown();
        }
      }
      if (isDoorOpen()) sys_status = ALARM;
      break;

    case KEEPALIVE:
      if (isDoorOpen()) {
        sys_status = ALARM;
      } else {
        keepalive();
      }
      break;

    case CORRCODE:
      if (isDoorOpen()) {
        sys_status = MANINSIDE;
        if (DEBUG) Serial.println("Porta aperta");
        inviodati("open");
        system_shutdown();
      }

      TIMEOUT = 60000;
      if (timeout() && enable_timeout ) {
        if (DEBUG)  Serial.println("\nCodice corretto ma porta non ancora aperta, spegnimento");
        if (deltat > 300) {
          if (DEBUG)  Serial.println("Torno in keepalive");
          sys_status = KEEPALIVE;
        }
        system_shutdown();
      }
      break;

    case MANINSIDE:
      if (!isDoorOpen()) {
        sys_status = MANOUTSIDE;
        payload = sys_status;
        inviodati(payload);
        sys_status = NOTDEFINED;
        if (DEBUG) Serial.println("man outside");
      }
      if (DEBUG) Serial.println("man still not outside");
      system_shutdown();
      break;

    case ALARM:
      if (!is_keepalive_boot) {
        if (isDoorOpen()) {
          payload = sys_status;
          inviodati(payload);
          system_shutdown();
        } else {
          sys_status = NORMAL;
        }
      } else {

        sys_status = KEEPALIVE;
      }


  }









}
