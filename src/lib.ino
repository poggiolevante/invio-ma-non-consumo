#include "config.h"

// Questa macro converte un 32-bit little-endian in un 32-bit big-endian
  #define Swap4Bytes(val) \
    ( (((val) >> 24) & 0x000000FF) | (((val) >>  8) & 0x0000FF00) | \
    (((val) <<  8) & 0x00FF0000) | (((val) << 24) & 0xFF000000) )


//Ritorna true se l'intervallo di tempo corrisponde ad un keepalive,
//con una tolleranza di 5 secondi.
  bool is_keepalive(){
  #ifdef DEBUG
    Serial.print("Boot time:   ");
    Serial.println(boot_time);
    Serial.print("EEPROM time: ");
    Serial.println(eeprom_time);
    Serial.print("Secondi da ultimo avvio: ");
    Serial.println(boot_time - eeprom_time);
  #endif
  long delta_t = boot_time - eeprom_time;
  if (delta_t < KEEPALIVE_INTERVAL){
         return (KEEPALIVE_INTERVAL - delta_t) < (5 + (TIMEOUT/1000));
  } else {
     return (delta_t - KEEPALIVE_INTERVAL) < (5 + (TIMEOUT/1000));
  }
}

// Routine di gestione di keepalive.
// Gestisce il contatore di keepalive e nel caso manda il messaggio.
void keepalive(){
  
  #ifdef DEBUG
    Serial.print("Numero keepalive: ");
    Serial.println(eeprom.read8(mem_keepalive));
  #endif
  
  if (eeprom.read8(mem_keepalive) == KEEPALIVE_ITERATIONS) {

     inviodati(false);
     eeprom.write8(mem_keepalive,0);
     
  } else {
    
    eeprom.write8(mem_keepalive,eeprom.read8(mem_keepalive)+1);
  
  }
  
  system_shutdown();

}


#ifdef INFO
//Stampa ora, data e temperatura dell'orologio
void echo_now(){
  DS3231_get(&t);
  Serial.print("Data:   ");
  if (t.mon < 10) Serial.print("0");
  Serial.print(t.mon); 
  Serial.print("/");
  if (t.mday < 10) Serial.print("0");
  Serial.print(t.mday);
  Serial.print("/");
  Serial.println(t.year);
  Serial.print("Ora :   ");
  if (t.hour < 10) Serial.print("0");
  Serial.print(t.hour);
  Serial.print(":");
  if (t.min < 10) Serial.print("0");
  Serial.print(t.min);
  Serial.print(":");
  if (t.sec < 10) Serial.print("0");
  Serial.println(t.sec);
  Serial.print("Temp orologio: ");
  Serial.println(DS3231_get_treg());
}
#endif


//Ritorna true se è stato raggiunto il timeout.
bool timeout(){  return abs(millis() - millid ) > TIMEOUT; }

//Evento che resetta il conteggio del timeout.
void reset_timeout(){  millid = millis(); }

//Ritorna true se la porta è aperta.
bool isDoorOpen(){ return digitalRead(pindoor); }

// Salva lo stato del sistema.
// Ritorna true se la scrittura sulla EEPROM ha successo.
bool saveStatus(status s){
  // E' inutile scrivere e consumare la memoria se
  // Lo stesso valore è già salvato.
  if (s != eeprom.read8(mem_status)) {
      eeprom.write8(mem_status, s);
      return eeprom.hasErrorOccurred();
  }
  return true;
}


//Carica lo stato dalla EEPROM e lo restituisce.
status loadStatus(){
  status s = (status) eeprom.read8(mem_status);
  if (s > MANOUTSIDE) {
    #ifdef INFO
      Serial.print("Attenzione: Stato ");
      Serial.print(s);
      Serial.println("non valido. EEPROM danneggiata?");
    #endif
    s = NOTDEFINED;
  }
  return s;
}


//Salva timestamp e stato e spegne il sistema.
void system_shutdown(){

  #ifdef INFO
  Serial.print("Spegnimento dispositivo in stato ");
  Serial.println(sys_status, HEX);
  #endif

  boot_time = t.unixtime;
  eeprom.write32(mem_time,boot_time);
  saveStatus(sys_status);

  digitalWrite (pinled,0);
  digitalWrite (pinready,0);
  
  digitalWrite(pinshutdown,HIGH);
  
  //In caso non ci sia uno spegnimento, simula una riaccensione programmata
  while (true) {
    LowPower.deepSleep(KEEPALIVE_INTERVAL * 1000);
    NVIC_SystemReset();
  }
  
}


//Inizializzatore dell'expander.
void expanderinit(){
    ioDevicePinMode(ioExpander, 0, OUTPUT);
    ioDevicePinMode(ioExpander, 1, OUTPUT);
    ioDevicePinMode(ioExpander, 2, OUTPUT);
    ioDevicePinMode(ioExpander, 3, OUTPUT);
    ioDevicePinMode(ioExpander, 4, INPUT);
    ioDevicePinMode(ioExpander, 5, INPUT);
    ioDevicePinMode(ioExpander, 6, INPUT);
    ioDevicePinMode(ioExpander, 7, INPUT);
    ioDeviceDigitalWrite(ioExpander, 0, 0);
    ioDeviceDigitalWrite(ioExpander, 1, 0);
    ioDeviceDigitalWrite(ioExpander, 2, 0);
    ioDeviceDigitalWrite(ioExpander, 3, 0);
}


void tone_startup(){
    tone(pinbuzzer,440,100);
    delay(110);
    tone(pinbuzzer,660,100);
    delay(110);
    tone(pinbuzzer,880,112);
}

void tone_code_correct(){
    tone(pinbuzzer,880,80);
    delay(80);
    tone(pinbuzzer,880,80);
}

void tone_code_incorrect(){
  for (int i=0; i<4 ; i++){
      tone(pinbuzzer,220,100);
      delay(50);
  }
}

bool isnum(char c){
  return ( c>=48 && c<=57 );
}

char toAscii(byte key){
    byte matrix[] = { 
                      '1', '2', '3', 'A',
                      '4', '5', '6', 'B',
                      '7', '8', '9', 'C',
                      '*', '0', '#', 'D',
                      };
  return matrix[15-(key-16)];
}


bool codicevalido(String c){
  if ( c.length()!=7 ) {
      return false;
  } else {
      for ( int i=1 ; i<6 ; i++ ) if (!(c[i]>=48 && c[i]<=57)) return false;
  }
  
  
  return true;
}

byte keyPressed(){
  byte key=255;
  ioDeviceSync(ioExpander);
  found=false;

  for (byte r=4; r<8; r++) {
    
    if (!ioDeviceDigitalRead(ioExpander,r)){
      
      for (byte c=0; c<4; c++) {
        
        ioDeviceDigitalWriteS(ioExpander,c,1);
      
        if(ioDeviceDigitalRead(ioExpander,r)){

          key = r*4+c;

          if (DEBUG > 2) {
            Serial.print("Key: ");
            Serial.print(key);
            Serial.print(", lastkey: ");
            Serial.println(lastkey);
            Serial.flush();
          }

          
          if (lastkey!=key){

          found=true; 

          tone(pinbuzzer,440,80);

          if (DEBUG >2) {
            Serial.print("Colonna: ");
            Serial.print(c);
            Serial.print(", Riga: ");
            Serial.print(r);
            Serial.print(", Raw key:");
            Serial.println(key);
          }
          
          }
          
          lastkey=key;
          
        }
        
        ioDeviceDigitalWrite(ioExpander,c,0);
        
        if(found) {
          c=4;
          r=8;
        } 
        
      }
      
    }
    
  }


  if (key==255) {
    lastkey=255;
  }

  return (found) ? toAscii(key) : 255 ;
  
}

void input_handler(){
    key = keyPressed();
  if (key!= 255) {
      
      if (DEBUG > 1) Serial.print(key);

      reset_timeout();

      //E' stato iniziato l'inserimento di un codice
      if (key == '*') {
        
        sys_status = PARTCODE;
        numbers = 0;
        
      } else if (isnum(key) && sys_status == PARTCODE) {
        if (DEBUG > 2 ) Serial.println(numbers);
          numbers++;
       } else if (key=='#' && sys_status == PARTCODE)
  
        {
         
         
         if (numbers==5)
         
            {
                digitalWrite (pinled,1);
                Serial.println ("\nIl codice è corretto");
                Serial.println("In attesa che la porta venga aperta");
                tone_code_correct();
                sys_status = CORRCODE;

                
            } else {
              
                Serial.println ("\nIl codice è errato");
                digitalWrite (pinled,0);
                tone_code_incorrect();
                sys_status = NOCODE;

            }

         numbers = 0;
         
         }
      else {
                Serial.println ("\nIl codice è errato");
                digitalWrite (pinled,0);
                tone_code_incorrect();
                sys_status = NOCODE;
     }

  }
  
 delay(30);

}
