#include "power.h"

void Power::begin(){
  ina219.begin();
  ina219.setCalibration_16V_400mA();
  readIna();
}

void Power::readIna(){
  ina219.powerSave(false);
  battery_voltage = ina219.getBusVoltage_V();
  if (battery_voltage < min_volt) {
    //todo:log:battery reading out of range, sensor broken?
    battery_voltage = min_volt;
  } else if (battery_voltage > max_volt) {
    //todo:log:battery range out
    battery_voltage = max_volt;
  }
  battery_amps = ina219.getCurrent_mA();
  ina219.powerSave(true);
}

float Power::getVoltage(){ return battery_voltage; }
float Power::getAmps(){ return battery_amps; }

bool Power::onBattery(){ return battery_voltage != min_volt; }
bool Power::isLowBattery(){ 
  if (onBattery()){
    return battery_voltage < low_volt;
    } else {
      return false;
    }
}
